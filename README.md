This repository contains projects that make use of the [rvapo](https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-vhdl) processor.

See subfolders of `/apps` for more details.