library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_hdl is
    port (
        DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
        DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
        DDR_cas_n : inout STD_LOGIC;
        DDR_ck_n : inout STD_LOGIC;
        DDR_ck_p : inout STD_LOGIC;
        DDR_cke : inout STD_LOGIC;
        DDR_cs_n : inout STD_LOGIC;
        DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
        DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_odt : inout STD_LOGIC;
        DDR_ras_n : inout STD_LOGIC;
        DDR_reset_n : inout STD_LOGIC;
        DDR_we_n : inout STD_LOGIC;
        FIXED_IO_ddr_vrn : inout STD_LOGIC;
        FIXED_IO_ddr_vrp : inout STD_LOGIC;
        FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
        FIXED_IO_ps_clk : inout STD_LOGIC;
        FIXED_IO_ps_porb : inout STD_LOGIC;
        FIXED_IO_ps_srstb : inout STD_LOGIC;
        CAN1_RXD : in std_logic;
        CAN2_RXD : in std_logic;
        CAN1_TXD : out std_logic;
        CAN2_TXD : out std_logic;
        FPGA_IO_A: inout std_logic_vector(10 downto 1);
        FPGA_IO_B: inout std_logic_vector(28 downto 13);
        FPGA_IO_C: inout std_logic_vector(40 downto 31)
    );
end entity;

architecture structure of top_hdl is
    type irqs_e is (
        IRQ_CTUCANFD_0,
        IRQ_CTUCANFD_1
    );

    type can_e is (
        CAN_CTUCANFD_0,
        CAN_CTUCANFD_1,
        CAN_XCAN_0,
        CAN_XCAN_1
    );

    type irq_arr_t is array(irqs_e) of std_logic;
    type can_tx_arr_t is array(can_e) of std_logic;

    --signal apbs : apb_arr_t;
    signal can_tx : can_tx_arr_t;
    signal can_rx : can_tx_arr_t;
    signal irqs : irq_arr_t;
    signal aclk : std_logic;
    signal arstn : std_logic;
    signal timestamp : std_logic_vector(63 downto 0);

    signal irq_f2p : std_logic_vector(3 downto 0);

    signal can_bus_tx              : std_logic_vector(3 downto 0);
    signal can_bus_rx              : std_logic_vector(3 downto 0);
    signal can_controller_tx       : std_logic_vector(7 downto 0);
    signal can_controller_rx       : std_logic_vector(7 downto 0);

    signal ADC_MISO : STD_LOGIC;
    signal ADC_MOSI : STD_LOGIC;
    signal ADC_SCLK : STD_LOGIC;
    signal ADC_SCS : STD_LOGIC;
    signal HAL_SENS : STD_LOGIC_VECTOR ( 1 to 3 );
    signal IRC_CHA : STD_LOGIC;
    signal IRC_CHB : STD_LOGIC;
    signal IRC_IDX : STD_LOGIC;
    signal PWM_OUT : STD_LOGIC_VECTOR ( 1 to 3 );
    signal PWM_SHDN : STD_LOGIC_VECTOR ( 1 to 3 );
    signal PWM_STAT : STD_LOGIC_VECTOR ( 1 to 3 );
    signal PWR_STAT : STD_LOGIC;
begin
    CAN1_TXD <= can_bus_tx(0);
    CAN2_TXD <= can_bus_tx(1);

    can_bus_rx(0) <= CAN1_RXD;
    can_bus_rx(1) <= CAN2_RXD;
    can_bus_rx(can_bus_rx'left downto 2) <= (others => '1');

    process(can_tx)
        variable i : can_e;
    begin
        can_controller_tx <= (others => '1');
        for i in can_e loop
            can_controller_tx(can_e'pos(i)) <= can_tx(i);
        end loop;
    end process;

    g_canrx: for i in can_e generate
        can_rx(i) <= can_controller_rx(can_e'pos(i));
    end generate;


    -- beware of concatenation and to/downto
    irq_f2p(0) <= irqs(IRQ_CTUCANFD_0);
    irq_f2p(1) <= irqs(IRQ_CTUCANFD_1);

    FPGA_IO_A(1) <= PWM_SHDN(1);
    FPGA_IO_A(3) <= PWM_OUT(1);
    FPGA_IO_A(5) <= PWM_SHDN(2);
    FPGA_IO_A(7) <= PWM_OUT(2);
    FPGA_IO_A(9) <= PWM_SHDN(3);
    FPGA_IO_B(13) <= PWM_OUT(3);

    PWM_STAT(1) <= FPGA_IO_B(15);
    PWM_STAT(2) <= FPGA_IO_B(17);
    PWM_STAT(3) <= FPGA_IO_B(19);

    PWR_STAT <= FPGA_IO_B(21);

    ADC_MISO <= FPGA_IO_B(22);
    FPGA_IO_B(23) <= ADC_MOSI;
    FPGA_IO_B(24) <= ADC_SCLK;
    FPGA_IO_B(25) <= ADC_SCS;

    IRC_CHA <= FPGA_IO_B(27);
    IRC_CHB <= FPGA_IO_C(31);
    IRC_IDX <= FPGA_IO_C(33);

    HAL_SENS(1) <= FPGA_IO_C(35);
    HAL_SENS(2) <= FPGA_IO_C(37);
    HAL_SENS(3) <= FPGA_IO_C(39);

    i_top: entity work.top_wrapper
    port map (
        ADC_MISO                  => ADC_MISO, 
        ADC_MOSI                  => ADC_MOSI,
        ADC_SCLK                  => ADC_SCLK,
        ADC_SCS                   => ADC_SCS,
        DDR_addr(14 downto 0)     => DDR_addr(14 downto 0),
        DDR_ba(2 downto 0)        => DDR_ba(2 downto 0),
        DDR_cas_n                 => DDR_cas_n,
        DDR_ck_n                  => DDR_ck_n,
        DDR_ck_p                  => DDR_ck_p,
        DDR_cke                   => DDR_cke,
        DDR_cs_n                  => DDR_cs_n,
        DDR_dm(3 downto 0)        => DDR_dm(3 downto 0),
        DDR_dq(31 downto 0)       => DDR_dq(31 downto 0),
        DDR_dqs_n(3 downto 0)     => DDR_dqs_n(3 downto 0),
        DDR_dqs_p(3 downto 0)     => DDR_dqs_p(3 downto 0),
        DDR_odt                   => DDR_odt,
        DDR_ras_n                 => DDR_ras_n,
        DDR_reset_n               => DDR_reset_n,
        DDR_we_n                  => DDR_we_n,
        FIXED_IO_ddr_vrn          => FIXED_IO_ddr_vrn,
        FIXED_IO_ddr_vrp          => FIXED_IO_ddr_vrp,
        FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
        FIXED_IO_ps_clk           => FIXED_IO_ps_clk,
        FIXED_IO_ps_porb          => FIXED_IO_ps_porb,
        FIXED_IO_ps_srstb         => FIXED_IO_ps_srstb,
        ctu_can_fd_0_can_rx       => can_rx(CAN_CTUCANFD_0),
        ctu_can_fd_1_can_rx       => can_rx(CAN_CTUCANFD_1),
        xcan_0_can_rx             => can_rx(CAN_XCAN_0),
        xcan_1_can_rx             => can_rx(CAN_XCAN_1),
        ctu_can_fd_0_can_tx       => can_tx(CAN_CTUCANFD_0),
        ctu_can_fd_1_can_tx       => can_tx(CAN_CTUCANFD_1),
        xcan_0_can_tx             => can_tx(CAN_XCAN_0),
        xcan_1_can_tx             => can_tx(CAN_XCAN_1),
        ctu_can_fd_0_irq          => irqs(IRQ_CTUCANFD_0),
        ctu_can_fd_1_irq          => irqs(IRQ_CTUCANFD_1),
        FCLK_CLK0_0               => aclk,
        FCLK_RESET0_N_0           => arstn,
        HAL_SENS                  => HAL_SENS,
        IRC_CHA                   => IRC_CHA,
        IRC_CHB                   => IRC_CHB,
        IRC_IDX                   => IRC_IDX,
        IRQ_F2P                   => irq_f2p,
        PWM_OUT                   => PWM_OUT,
        PWM_SHDN                  => PWM_SHDN,
        PWM_STAT                  => PWM_STAT,
        PWR_STAT                  => PWR_STAT,
        TIMESTAMP                 => timestamp,
        can_bus_tx                => can_bus_tx,
        can_bus_rx                => can_bus_rx,
        can_controller_tx         => can_controller_tx,
        can_controller_rx         => can_controller_rx
    );

end architecture;
