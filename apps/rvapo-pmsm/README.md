This project combines the [CTU CAN project](https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top/-/tree/mz_apo-2x-xcan-4x-ctu?ref_type=heads) with the [rvapo processor](https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-vhdl)
and peripherals for motor control with [pxmc library](https://gitlab.fel.cvut.cz/otrees/motion/pxmc-linux/-/tree/mz_apo-pmsm-rvapo?ref_type=heads) on the [mzapo board](https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo/start).

Use `make project` to recreate the Vivado 2018.2 project and `make bitstream` to generate bitstream.

Software and documentation can be found in the rvapo repository, because this project is but an application of it.

**Change in addresses:**
Due to clash of [zlogan](https://github.com/eltvor/zlogan/tree/zlogan-component-dev) and rvapo addresses, the rvapo control registers have been moved from `0x43c00000` to `0x43c40000`. This affects all programs that control the processor:
* pxmc-linux (in zynq_pmsm_rvapo_mc.c::z2dcdrv1_init, where motor commutation program is loaded into the processor)
* run.sh
* fpgatest.sh
* debugger

**Block design updates and export:**
The `top.tcl` update/editing in Vivado should be finished
by double-click to the block design and run
```
  File -> Export -> Export Block Design...
```
and the `top.tcl` at the level of the project `recreate.ctl` should be updated.
